﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsControlLibrary1
{
    public partial class digitalClock: UserControl
    {
        public digitalClock()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            hourMinuteLabel.Text = DateTime.Now.ToString("HH:mm");
            secondsLabel.Text = DateTime.Now.ToString("ss");

        }

        private void hourMinuteLabel_Click(object sender, EventArgs e)
        {

        }

        private void digitalClock_Load(object sender, EventArgs e)
        {
            timer1.Start();
        }
    }
}
