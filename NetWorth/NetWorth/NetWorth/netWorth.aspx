﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="netWorth.aspx.cs" Inherits="NetWorth.netWorth" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            How old are you?&nbsp;
            <asp:TextBox ID="ageBox" runat="server" style="margin-bottom: 0px"></asp:TextBox>
            <br />
            <br />
            How much are you worth?
            <asp:TextBox ID="worthBox" runat="server" style="margin-bottom: 0px"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="checkButton" runat="server" OnClick="checkButton_Click" Text="Please click here to be judged " />
            <br />
            <br />
            <asp:Label ID="outputLabel" runat="server"></asp:Label>
        </div>
    </form>
</body>
</html>
