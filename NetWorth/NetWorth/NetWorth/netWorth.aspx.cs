﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetWorth
{
    public partial class netWorth : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void checkButton_Click(object sender, EventArgs e)
        {
            string ageReceived = ageBox.Text;
            string worthReceived = ageBox.Text;

            outputLabel.Text = "At " + ageReceived + " years old,"+ " you should have more than " + "$" + worthReceived +" saved up!";
        }
    }
}