﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Id.aspx.cs" Inherits="WebApps101.Id" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            First Name:&nbsp;
            <asp:TextBox ID="firstNameBox" runat="server"></asp:TextBox>
        </div>
        <p>
            Second Name:
            <asp:TextBox ID="lastNameBox" runat="server"></asp:TextBox>
        </p>
        <p>
            <asp:Button ID="submitButton" runat="server" Height="26px" OnClick="submitButton_Click" Text="Click To Submit" />
        </p>
        <p>
            <asp:Label ID="resultLabel" runat="server"></asp:Label>
        </p>
    </form>
</body>
</html>
